// Compiled by ClojureScript 0.0-2740 {}
goog.provide('quil_explore.core');
goog.require('cljs.core');
goog.require('quil.middleware');
goog.require('quil.core');
quil_explore.core.setup = (function setup(){
quil.core.frame_rate.call(null,(60));

quil.core.color_mode.call(null,new cljs.core.Keyword(null,"hsb","hsb",-753472031));

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"color","color",1011675173),(0),new cljs.core.Keyword(null,"angle","angle",1622094254),(0)], null);
});
quil_explore.core.update_state = (function update_state(state){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"color","color",1011675173),cljs.core.mod.call(null,(new cljs.core.Keyword(null,"color","color",1011675173).cljs$core$IFn$_invoke$arity$1(state) + (1)),(30)),new cljs.core.Keyword(null,"angle","angle",1622094254),(new cljs.core.Keyword(null,"angle","angle",1622094254).cljs$core$IFn$_invoke$arity$1(state) + 0.02)], null);
});
quil_explore.core.draw_state = (function draw_state(state){
quil.core.background.call(null,(0));

quil.core.fill.call(null,new cljs.core.Keyword(null,"color","color",1011675173).cljs$core$IFn$_invoke$arity$1(state),(255),(255));

var angle = new cljs.core.Keyword(null,"angle","angle",1622094254).cljs$core$IFn$_invoke$arity$1(state);
var x = ((100) + ((100) * quil.core.sin.call(null,angle)));
var y = ((30) * quil.core.tan.call(null,angle));
var tr__5763__auto__ = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(quil.core.width.call(null) / (3)),(quil.core.height.call(null) / (2))], null);
quil.core.push_matrix.call(null);

quil.core.translate.call(null,tr__5763__auto__);

quil.core.ellipse.call(null,x,y,(100),(100));

return quil.core.pop_matrix.call(null);
});
quil_explore.core.quil_explore = (function quil_explore__$1(){
return quil.sketch.sketch.call(null,new cljs.core.Keyword(null,"draw","draw",1358331674),quil_explore.core.draw_state,new cljs.core.Keyword(null,"middleware","middleware",1462115504),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [quil.middleware.fun_mode], null),new cljs.core.Keyword(null,"setup","setup",1987730512),quil_explore.core.setup,new cljs.core.Keyword(null,"size","size",1098693007),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(500),(500)], null),new cljs.core.Keyword(null,"update","update",1045576396),quil_explore.core.update_state,new cljs.core.Keyword(null,"host","host",-1558485167),"quil-explore");
});
goog.exportSymbol('quil_explore.core.quil_explore', quil_explore.core.quil_explore);

if(cljs.core.truth_(cljs.core.some.call(null,(function (p1__5304__5305__auto__){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"no-start","no-start",1381488856),p1__5304__5305__auto__);
}),null))){
} else {
quil.sketch.add_sketch_to_init_list.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"fn","fn",-1175266204),quil_explore.core.quil_explore,new cljs.core.Keyword(null,"host-id","host-id",742376279),"quil-explore"], null));
}
