(require 'cljs.build.api)

(cljs.build.api/watch "src"
  {:main 'quil-explore.core
   :output-to "js/main.js"})
